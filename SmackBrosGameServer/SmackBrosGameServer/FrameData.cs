﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmackBrosGameServer
{
    public struct FrameData
    {
        public int NumJumps;
        public int RunStartUp;
        public int JumpStartUp;
        public int JabDuration;
        public int JabComboDuration;
        public int DashAttackDuration;
        public int FSmashStartup;
        public int USmashStartup;
        public int DSmashStartup;
        public int FSmashDuration;
        public int USmashDuration;
        public int DSmashDuration;
        public int FTiltStartup;
        public int UTiltStartup;
        public int DTiltStartup;
        public int FTiltDuration;
        public int UTiltDuration;
        public int DTiltDuration;
        public int DownGetupAttackDuration;
        public int UpGetupAttackDuration;
        public int FairEndLag;
        public int UairEndLag;
        public int DairEndLag;
        public int BairEndLag;
        public int FairDuration;
        public int UairDuration;
        public int DairDuration;
        public int BairDuration;
    }
}
